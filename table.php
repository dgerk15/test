<?php
/**
 * Created by PhpStorm.
 * User: dgerk
 * Date: 27.05.2016
 * Time: 18:31
 */
    require_once "config.php";
    function table(){
        
        $mysqli = new mysqli(SQL_HOST, SQL_USER, SQL_PASSWORD, SQL_DB);
    
        if ($mysqli -> connect_errno){
            echo "Не удалось подключиться к БД : (" . $mysqli -> connect_errno . ")" . $mysqli -> connect_error;
        }
    
        $query = "SELECT * FROM USERS";
    
        $result = $mysqli -> query($query);
    
        $row = "";
    
        while ($row = $result -> fetch_array(MYSQLI_ASSOC)){
            $res [] = $row;
        } 
        
        echo json_encode($res);

    }
    table();