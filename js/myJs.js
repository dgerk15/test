
/**
 * Created by dgerk on 28.04.2016.
 */

$ (document).ready(function () {

    $("#computeFibonacci").click(function () {

        var inputNumber = $("#inputFibonacci").val();
        // alert(fibonacci(inputNumber));
        $.post("computeFibonacci.php", {n: $("#inputFibonacci").val()}, function (data) {

            // var fib = [];
            // for (var i = 0; i < data.length; i++){
            //     fib[i] = data[i] + " ";
            // }

            alert(data);
        })

    })



    $("#addUserBtn").click(function () {
        fillUserEditDialog();
        $("#userEditDlg").modal({
            backdrop: true
        })
    })

    $("#btnUserSaveDlg").click(function () {
        // fillUserEditDialog();
        $.post("save.php", {
            id: $("#userIdDlg").val(), name: $("#userNameDlg").val(),
            email: $("#userEmailDlg").val(), password: $("#userPasswordDlg").val(), phone: $("#userPhoneDlg").val()
        }, function (data) {
            if (data) {
                viewTable();
            }
            return false;
        })

    })

    viewTable();

})



    function viewTable() {
        
        $.post("table.php", "", function (result) {

            if (result) {

            var data = JSON.parse(result);
            }

        $("#tableBody").html(function () {

            var strTab = "";

            for (var i = 0; i < data.length; i++) {

                var count = parseInt(i+1);
                strTab += "<tr trId=" + data[i].USER_ID + ">";
                strTab += "<td class='count'>" + count + "</td>";
                strTab += "<td class='hidden password'>" + data[i].PASSWORD + "</td>";
                strTab += "<td class='name'>" + data[i].NAME + "</td>";
                strTab += "<td class='email'>" + data[i].EMAIL + "</td>";
                strTab += "<td class='phone'>" + data[i].PHONE + "</td>";
                strTab += "<td class='delete'>" +
                // strTab += "<td class='delete' userId=" + data[i].USER_ID + ">" +
                    "<button class='btn btn-default userDelete' title='Удалить'>" +
                    "<span class='glyphicon glyphicon-trash'></span>" +
                    "</button>" +
                    "&nbsp" +
                    "<button class='btn btn-default userEdit' title='Изменить'>" +
                    "<span class='glyphicon glyphicon-pencil'></span>" +
                    "</button>" + "</td>";
                strTab += "</tr>";

            }

            return strTab;

        })
            bindDeleteClick($("#tableForm .userDelete"));
            bindEditClick($("#tableForm .userEdit"));
            return false;
        })
    }

        //1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144, 233, 377, 610, 987
    // function fibonacci(n) {
    //
    //
    //     var a = 1;
    //     var b = 1;
    //     var c = 0;
    //     var arr = [];
    //     arr[0]=1;
    //     arr[1]=1;
    //     for (var i = 2; i < n; i++) {
    //
    //         c = a + b;
    //         a = b;
    //         b = c;
    //         arr[i] = c;
    //     }
    //
    //     return arr;
    // }

function bindDeleteClick(elements) {
    elements.on("click", function () {
        var userId = $(this).parents("tr").attr("trId");
        $.post("delete.php", {id: userId}, function (data) {
            viewTable();
            return false;
        })
    })
}

function bindEditClick(elements) {
    elements.on("click", function () {
        fillUserEditDialog($(this).parents("tr"));
        $("#userEditDlg").modal({
            backdrop: true
        })
            // viewTable();
            return false;
        })

}

function fillUserEditDialog(tr) {
    if (tr) {
        $("#userIdDlg").val(tr.attr("trId"));
        $("#userNameDlg").val(tr.children("td.name").text());
        $("#userEmailDlg").val(tr.children("td.email").text());
        $("#userPasswordDlg").val(tr.children("td.password").text());
        $("#userPhoneDlg").val(tr.children("td.phone").text());
    }
    else {
        $("#userIdDlg").val('');
        $("#userNameDlg").val('');
        $("#userEmailDlg").val('');
        $("#userPasswordDlg").val('');
        $("#userPhoneDlg").val('');

    }
}





